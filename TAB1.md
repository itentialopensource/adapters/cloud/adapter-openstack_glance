# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Openstack_glance System. The API that was used to build the adapter for Openstack_glance is usually available in the report directory of this adapter. The adapter utilizes the Openstack_glance API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The OpenStack Glance adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Glance, an image service in OpenStack. With this adapter you have the ability to perform operations such as:

- Create, update, manage or remove images.
- Manage image members.


For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
