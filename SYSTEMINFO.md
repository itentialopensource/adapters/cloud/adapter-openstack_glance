# OpenStack Glance

Vendor: OpenStack
Homepage: https://www.openstack.org/

Product: Glance (an image service in OpenStack)
Product Page: https://docs.openstack.org/glance/latest/

## Introduction
We classify OpenStack Glance into the Cloud domain as Glance provides the capabilities to manage images within the OpenStack cloud


## Why Integrate
The OpenStack Glance adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Glance, an image service in OpenStack. With this adapter you have the ability to perform operations such as:

- Create, update, manage or remove images.
- Manage image members.


## Additional Product Documentation
The [API documents for OpenStack Glance](https://docs.openstack.org/api-ref/image/v2/index.html)

