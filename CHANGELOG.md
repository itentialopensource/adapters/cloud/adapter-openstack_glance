
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:55PM

See merge request itentialopensource/adapters/adapter-openstack_glance!11

---

## 0.3.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-openstack_glance!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:03PM

See merge request itentialopensource/adapters/adapter-openstack_glance!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:16PM

See merge request itentialopensource/adapters/adapter-openstack_glance!7

---

## 0.3.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!6

---

## 0.2.4 [03-28-2024]

* Changes made at 2024.03.28_13:11PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!5

---

## 0.2.3 [03-21-2024]

* Changes made at 2024.03.21_13:42PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_15:29PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!3

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:39AM

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!2

---

## 0.2.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-openstack_glance!1

---

## 0.1.1 [09-05-2022]

* Bug fixes and performance improvements

See commit f2025aa

---
